![salute](https://readme-typing-svg.demolab.com/?lines=HI%20there%20👋,%20I'm%20Mi-guel)
>> #### **Status**
> Digging OSINT

## 🐵 🙈 🙉 🙊
I moved from [GitHub](https://github.com/miguebarbell) to here, there is more kind a social media thing 🎭, means less privacy.
I also don't like GitHub copilot, makes developer not lazy, stupid.

If you go to [my web page](https://miguel.debloat.us) you will see pictures of lana, **my dog** 🐕.


| Skill | Level | Notes        |
| ------------  | ------ |--------------|
| Python  		| 🍍🍍🍍🍍 	| improve pythonic   	|
| JavaScript 	| 🍍🍍🍍🍍 	|              				|
| Linux 			| 🍍🍍🍍🍍 	| I love it!   				|
| Bash  			| 🍍🍍🍍🍍 	| UNIX Philosophy			|
| Rust  			| 🍍 				| Just started 				|
| ML 					| 🍍🍍🍍 		| It's fun     				|
| React  			| 🍍🍍🍍🍍 	|              				|
| nodejs  		| 🍍🍍🍍🍍 	|              				|

📫 Talk to me, send me an [email](mailto:miguel@debloat.us).

### I left you some pictures about things I'm proud of. 🐾👣

#### [React and redux](https://bsa.debloat.us) skills (try buying something)

<img alt="hero image" src="https://miguel.debloat.us/static/images/fullstack/hero.png" width="500" />
<img alt="adding item to the card" src="https://miguel.debloat.us/static/images/fullstack/item.png" width="500"/>

#### My [blog](https://life.debloat.us)
<img alt="blog" src="https://miguel.debloat.us/static/images/fullstack/heroblog.png" width="500"/>
<img alt="blog" src="https://miguel.debloat.us/static/images/fullstack/postsblog.png" width="500"/>

### Some deeplearning applications.

#### This recognize all the faces in a picture and set a score to the smile.

<img alt="sympathy app" src="https://miguel.debloat.us/static/images/deeplearning/gifs.gif" width="500"/>

#### Here you can annonimize any picture, and choose the filter that you want.

<img alt="annonymazer app" src="https://miguel.debloat.us/static/images/deeplearning/gifa.gif" width="500"/>

### And a height stimator, augmented reality with aruco tags.

<img alt="height stimator" src="https://miguel.debloat.us/static/images/deeplearning/gifh.gif" width="500"/>
